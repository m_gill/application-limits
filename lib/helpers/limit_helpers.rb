module LimitHelpers
	class << self

		# PAGINATION LIMIT OF 100
		def closed_issues(page = 1)
			s = HTTParty.get("https://gitlab.com/api/v4/groups/9970/issues?labels=Application%20Limits&state=closed&per_page=100&page=#{page}", headers: { "PRIVATE-TOKEN" => ENV['GITLAB_ACCESS_TOKEN'] })
			s.parsed_response
		end

		# PAGINATION LIMIT OF 100
		def created_issues(since_date, page = 1)
			s = HTTParty.get("https://gitlab.com/api/v4/groups/9970/issues?labels=Application%20Limits&state=opened&order_by=created_at&sort=desc&per_page=30&page=#{page}", headers: { "PRIVATE-TOKEN" => ENV['GITLAB_ACCESS_TOKEN'] })
			s.parsed_response.select {|x| since_date < Date.parse(x['created_at']) }
		end

		# PAGINATION LIMIT OF 100
		def all_open_issues(page = 1)
			s = HTTParty.get("https://gitlab.com/api/v4/groups/9970/issues?labels=Application%20Limits&state=opened&per_page=100&page=#{page}", headers: { "PRIVATE-TOKEN" => ENV['GITLAB_ACCESS_TOKEN'] })
			s.parsed_response
		end

		# PAGINATION LIMIT OF 100
		def all_closed_issues(page = 1)
			s = HTTParty.get("https://gitlab.com/api/v4/groups/9970/issues?labels=Application%20Limits&state=closed&per_page=100&page=#{page}", headers: { "PRIVATE-TOKEN" => ENV['GITLAB_ACCESS_TOKEN'] })
			s.parsed_response
		end
	end
end