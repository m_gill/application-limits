# frozen_string_literal: true
require 'httparty'
require 'byebug'
require 'active_support/all'
require 'yaml'
require 'chronic'
require 'tempfile'
require 'csv'

# Autoloader (Relative to Gem Root)
lib_root = File.dirname(File.absolute_path(__FILE__))
Dir.glob("#{lib_root}/**/*.rb", &method(:require))