require_relative "./lib/autoloader.rb"

if ARGV.empty?
  puts
  puts
  puts
  puts
  puts "Please use this as `ruby limits.rb 2020-05-01`"
  puts
  puts
  puts
  puts
  exit
end
COMPARE_DATES = ARGV[0].split('-')

compare_date = Date.new(COMPARE_DATES[0].to_i, COMPARE_DATES[1].to_i, COMPARE_DATES[2].to_i)



closed_issues = LimitHelpers.closed_issues

closed_since_last = closed_issues.select {|x| compare_date < Date.parse(x['closed_at']) }	

created_issues_since = LimitHelpers.created_issues(compare_date)

all_open_issues = []
all_closed_issues = []

# All open issues
3.times { |time| LimitHelpers.all_open_issues(time + 1).map {|x| all_open_issues << x } }

# All closed issues
3.times { |time| LimitHelpers.all_closed_issues(time + 1).map {|x| all_closed_issues << x } }


# Scheduled issues
# milestone is not null, or milestone is not backlog
scheduled_issues = all_open_issues.select {|x| x['milestone'] && !x['milestone'].nil? && x['milestone']['title'] != 'Backlog' }

unscheduled_issues = all_open_issues.select {|x| x['milestone'].nil? || x['milestone']['title'] == 'Backlog' }


puts closed_since_last

puts "- Total Issues: #{all_open_issues.count + all_closed_issues.count} (+#{created_issues_since.count})"
puts "- Complete: #{all_closed_issues.count} (+#{closed_since_last.count})"
puts "- Backlog/Unscheduled: #{unscheduled_issues.count}"
puts "- Scheduled: #{scheduled_issues.count}"

puts "----"

closed_since_last.each do |c|
  puts c['_links']['self']
  puts c['title']
end