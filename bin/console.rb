#!/usr/bin/env ruby

require "bundler"

# Require the development libs via Bundler
Bundler.require(:default, :development)

require_relative "../lib/autoloader.rb"
require "irb"

IRB.start(__FILE__)
