# Application Limits

### Description

Runs a simple report on progress for the Application Limits initiative using the GitLab API.


### Usage

1. Clone this repo
1. `cd` into the project locally
1. `bundle install`
1. `GITLAB_ACCESS_TOKEN=123456 ruby limits.rb 2021-01-01`

Use the command below to generate numbers based on the last time the report was run:

`GITLAB_ACCESS_TOKEN=xxx ruby limits.rb xxxx-xx-xx`

